class V1::ProjectsController < ApplicationController
  before_action :set_project, only: [:show, :update, :destroy]

  # GET /projects
  def index
    @projects = Project.page(params[:page]).per(10)
    @projects_serializer = ProjectSerializer.new(@projects).serializable_hash
    json_response(@projects_serializer)
  end

  # POST /projects
  def create
    @project = Project.create!(project_params)
    @project_serializer = ProjectSerializer.new(@project).serializable_hash
    json_response(@project_serializer, :created)
  end

  # GET /projects/:id
  def show
    @project_serializer = ProjectSerializer.new(@project).serializable_hash
    json_response(@project_serializer)
  end

  # PUT /projects/:id
  def update
    @project.update(project_params)
    head :no_content
  end

  # DELETE /projects/:id
  def destroy
    @project.destroy
    head :no_content
  end

  private

    def project_params
      # whitelist params
      params.permit(:name, :description)
    end

    def set_project
      @project = Project.find(params[:id])
    end
end
