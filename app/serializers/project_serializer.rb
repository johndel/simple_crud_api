class ProjectSerializer
  include FastJsonapi::ObjectSerializer
  attributes :name, :description
end
