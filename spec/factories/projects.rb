FactoryBot.define do
  factory :project do
    name { Faker::Superhero.name }
    description { Faker::Superhero.descriptor }
  end
end