require 'rails_helper'

# Test suite for the Project model
RSpec.describe Project, type: :model do
  # Association test
  # ensure Project model has a 1:m relationship with the Item model
  it { should validate_presence_of(:name) }
end