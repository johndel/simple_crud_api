require 'rails_helper'

RSpec.describe 'Projects API', type: :request do
  # initialize test data
  let!(:projects) { create_list(:project, 10) }
  let(:project_id) { projects.first.id }

  # Test suite for GET /v1/projects
  describe 'GET /v1/projects' do
    # make HTTP get request before each example
    before { get '/v1/projects' }

    it 'returns projects' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json['data'].size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end
  end

  # Test suite for GET /v1/projects/:id
  describe 'GET /v1/projects/:id' do
    before { get "/v1/projects/#{project_id}" }

    context 'when the record exists' do
      it 'returns the project' do
        expect(json).not_to be_empty
        expect(json['data']['id']).to eq(project_id.to_s)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:project_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Project/)
      end
    end
  end

  # Test suite for POST /v1/projects
  describe 'POST /v1/projects' do
    # valid payload
    let(:valid_attributes) { { name: 'Learn Ruby', description: '1' } }

    context 'when the request is valid' do
      before { post '/v1/projects', params: valid_attributes }

      it 'creates a project' do
        expect(json['data']['attributes']['name']).to eq('Learn Ruby')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/v1/projects', params: { name: '' } }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Name can't be blank/)
      end
    end
  end

  # Test suite for PUT /v1/projects/:id
  describe 'PUT /v1/projects/:id' do
    let(:valid_attributes) { { name: 'Shopping' } }

    context 'when the record exists' do
      before { put "/v1/projects/#{project_id}", params: valid_attributes }

      it 'updates the record' do
        expect(response.body).to be_empty
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end
    end
  end

  # Test suite for DELETE /v1/projects/:id
  describe 'DELETE /v1/projects/:id' do
    before { delete "/v1/projects/#{project_id}" }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end
  end
end